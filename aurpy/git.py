"""This package provides helpers for working with git."""

from pathlib import Path
from typing import Union


def find_root_folders(base: Union[str, Path]):
    """Finds and yields folders that are a git project root."""
    if isinstance(base, str):
        base = Path(base)

    for candidate in base.iterdir():
        if candidate.is_dir() and candidate.parts[-1] == ".git":
            yield candidate.parent
        elif candidate.is_dir():
            yield from find_root_folders(candidate)
