# pylint: disable=missing-module-docstring

# This is a transcription of a bash program:
# https://github.com/AladW/aurutils/commit/7ca83bcf2d17819ed46ad02eed8f1a412cf6ab4a

import os
import subprocess
from os import path

from xdg import XDG_CACHE_HOME

from aurpy import git


def main():  # pylint: disable=missing-function-docstring
    aurdest = os.getenv("AURDEST")
    if not aurdest:
        aurdest = path.join(XDG_CACHE_HOME, "aurutils", "sync")

    for folder in git.find_root_folders(aurdest):
        subprocess.run(["git", "clean", "-xf"], cwd=folder, check=True)

    print("Searching for folders not having a PKGBUILD")
    # TODO: implement the following bash code in python
    # Print directories which do not contain a PKGBUILD file
    # grep -Fxvf <(find "${AURDEST}" -maxdepth 2 -name PKGBUILD -printf '%h\n') \
    #     <(printf '%s\n' "${AURDEST}"/*)
