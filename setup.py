#!/usr/bin/env python

# pylint: disable=missing-module-docstring

from pathlib import Path

from setuptools import find_packages, setup

# For a nice example "setup.py" take a look at
# https://github.com/pypa/sampleproject/blob/master/setup.py


def scripts():  # pylint: disable=missing-function-docstring
    for file in Path("./aurpy/bin").iterdir():
        name = file.stem
        if not name.startswith("__"):
            yield "aur-{0}=aurpy.bin.{0}:main".format(name)


setup(
    name="aurpy",
    version="0.0.1",
    description="AUR utils helpers",
    packages=find_packages(),
    python_requires=">=3.8",
    install_requires=["xdg==4.0.1"],
    extras_require={},
    entry_points={"console_scripts": list(scripts())},
)
