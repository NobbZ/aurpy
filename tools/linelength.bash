#!/usr/bin/env bash

length=${1}
shift

declare -A result

while (( $# )); do
  l=$(wc -L ${1} | cut -d' ' -f1)
  if (( ${l} > ${length} )); then
    result[${1}]=${l}
  fi
  shift
done

for file in "${!result[@]}"; do
  printf "%s:\t%d\n" "${file}" "${result[$file]}"
done

exit ${#result[@]}