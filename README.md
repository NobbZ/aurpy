AURpy
=====

Some small tools building on top of [`aurutils`](https://github.com/AladW/aurutils), tailored to my own setup.

Hacking
-------

Please make sure you have [`lefthook`](https://github.com/Arkweid/lefthook) installed. It is used to configure commit
hooks.

Also we need some python dependencies installed: `pytest`, `mypy`, `pylint`, `black`, and `isort`.

```sh
$ pip install pytest mypy pylint black isort
$ lefthook install
$ ./setup.py develop
```
